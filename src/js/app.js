import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const profile = document.querySelector("profile");
  const image = document.querySelector(".image");
  profile.addEventListener("click", () => {
	profile.className = "active";
	image.style = "transform: scale(2, 2); -ms-transform: scale(2, 2); -webkit-transform: scale(2, 2);";
	
  });
});
